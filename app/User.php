<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasPushSubscriptions,
        Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    CONST ADMINS = [1];
    CONST WRITERS = [2];
    CONST EDITORS = [3];
    CONST REVIEWER = [4];
    CONST FINANCES = [5];
    CONST MARKETINGS = [6];
    CONST REVIEWERILMIAH = [7];
    CONST SALES = [8];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'name_limited',
    ];

    /**
     * Get the name limited attribute.
     *
     * @param string|null $value
     * @return string
     */
    public function getNameLimitedAttribute($value)
    {
        return str_limit(ucwords(mb_strtolower($this->attributes['name'])), 15);
    }
    
    public function books()
    {
      return $this->belongsToMany(Book::class);
    }

    public function role()
    {
      return $this->belongsTo(Role::class);
    }

    public function messages()
    {
      return $this->hasMany(\App\Message::class);
    }

    public function isAdmin()
    {
      return in_array($this->role_id, User::ADMINS);
    }

    public function isEditor()
    {
      return in_array($this->role_id, User::EDITORS);
    }

    public function isReviewer()
    {
      return in_array($this->role_id, User::REVIEWER);
    }

    public function isReviewerIlmiah()
    {
      return in_array($this->role_id, User::REVIEWERILMIAH);
    }

    public function isWriter()
    {
      return in_array($this->role_id, User::WRITERS);
    }

    public function isFinance()
    {
      return in_array($this->role_id, User::FINANCES);
    }

    public function isMarketing()
    {
      return in_array($this->role_id, User::MARKETINGS);
    }

    public function isSales()
    {
      return in_array($this->role_id, User::SALES);
    }

    public function scopeAllReviewer($query)
    {
      return $query->whereHas("role",function($query){
        $query->where("name","Reviewer");
      });
    }

    public function scopeAllReviewerIlmiah($query)
    {
      return $query->whereHas("role",function($query){
        $query->where("name","reviewer ilmiah");
      });
    }

    public function scopeEditors($query)
    {
        return $query->whereHas('role', function($query) {
            $query->whereIn('id', User::EDITORS);
        });
    }

    public function scopeWriters($query)
    {
        return $query->whereHas('role', function($query) {
            $query->whereIn('id', User::WRITERS);
        });
    }
}
