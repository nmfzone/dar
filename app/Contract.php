<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{

  public function book()
  {
    return $this->hasOne(Book::class);
  }

  public function notifications()
  {
    return $this->hasMany(Notification::class);
  }


}
