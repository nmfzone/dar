<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;

class ChatController extends Controller
{
    public function index()
    {
      return view('chat.index');
    }
    public function getMessages()
    {
      return Chat::with('user')->orderBy('id','desc')->take(10)->get();
    }
}
