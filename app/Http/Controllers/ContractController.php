<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;
use App\Contract;
use App\Notification;
use Illuminate\Support\Facades\Storage;

class ContractController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(auth()->user()->isWriter()) {
         $books = Book::where('user_id', auth()->id())->orderBy('id','desc')->get();
      }elseif (auth()->user()->isEditor()) {
        $books = Book::all();
      }
        $contracts = Contract::all();
        return view('contracts.index', compact(['books','contracts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $books = Book::all();
      $contracts = Contract::all();
      return view('contracts.upload', compact(['books','contracts']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
      $request->validate([
        'book_id'=>'required',
        'royalti'=>'required'
      ]);
      // return $request->all();
      $contract = new Contract;
      $contract->kontrak = $request->kontrak;
      $contract->book_id = $request->book_id;
      $contract->royalti = $request->royalti;
      $contract->save();

      return redirect(route('contracts'))->with('status', 'Contract was uploaded!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract, $book_id)
    {
        // $books = Book::all();
        $contract = Contract::where('book_id',$book_id)->first();

        if (empty($contract)) {
          $book = Book::where('id', $book_id)->first();
          session(['book' => $book]);
          return redirect(route('contracts.upload'));
        }

        return view('contracts.edit',compact(['books','contract']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contract)
    {
        $contract = Contract::find($contract);
        $contract->kontrak=$request->kontrak;
        $contract->royalti=$request->royalti;
        $contract->save();
        return redirect(route('contracts'))->with('status','Contract Upadeted Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
