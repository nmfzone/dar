<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;
use App\Sale;
use App\Notification;
use App\Contract;

class SaleController extends Controller
{
    public function upload()
    {
      $books = Book::where('status','5')->get();
      return view('sales.upload', compact('books'));
    }

    public function store(Request $request)
    {
      $sale = new Sale;
      $sale->invoice = time();
      $sale->tanggal = $request->tanggal;
      $sale->seller_id =  auth()->id();
      $sale->save();

      $data = array();
      $jumlah = $request->jumlah;
      $harga  = $request->harga;
      foreach ($request->judul as $key => $val) {
        $data[] = array(
          'book_id' => $val,
          'jumlah'  => $jumlah[$key],
          'harga'   => $harga[$key]
        );
      }

      $sale->books()->sync($data);
      return redirect(route('sales.index'))->with('status', 'Sale was updated!');
    }
}
