<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\User;
use App\Events\ChatCreated;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ChatsController extends Controller
{
    /**
     * Create the chat.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|min:1|max:1000',
            'receiver_id' => [
                'required',
                'exists:users,id',
                Rule::notIn([auth()->user()->id]),
            ]
        ], [
            'receiver_id.not_in' => 'Cannot send chat to youself!',
        ]);

        $chat = Chat::forceCreate([
            'content' => $request->get('content'),
            'sender_id' => auth()->user()->id,
            'receiver_id' => $request->get('receiver_id'),
        ])->load('sender', 'receiver');

        event(new ChatCreated($chat));

        return $chat;
    }

    /**
     * List of the user for conversation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function conversation(Request $request)
    {
        if (auth()->user()->isEditor()) {
            return User::writers()->get();
        } else if (auth()->user()->isWriter()) {
            return User::editors()->get();
        }

        return response()->json([]);
    }

    /**
     * Show the conversation between authenticated user and the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function showConversation(Request $request, User $user)
    {
        if (auth()->user()->is($user)) {
            abort(404);
        }

        $chats = Chat::where(function ($query) use ($user) {
                $query->whereSenderId($user->id)
                    ->whereReceiverId(auth()->user()->id);
            })->orWhere(function ($query) use ($user) {
                $query->whereSenderId(auth()->user()->id)
                    ->whereReceiverId($user->id);
            })
            ->with('sender', 'receiver')
            ->limit(30)
            ->latest()
            ->get();

        return $chats->sortBy('created_at')->values();
    }
}
