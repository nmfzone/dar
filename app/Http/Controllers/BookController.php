<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;
use App\User;
use App\Notification;
use App\Role;
use Illuminate\Support\Facades\Storage;



class BookController extends Controller
{
    public function upload(Request $request)
    {
       // return $request->all();
      $request->validate([
        'title'=>'required|min:2',
        'caption'=>'required'
      ]);
      // return $request->all();

      $file = $request->file('filename');

      $book = new Book;
      $book->title = $request->title;
      $book->caption = $request->caption;
      $book->path = $file->store('file','public');
      $book->filename = $file->getClientOriginalName();
      $book->status = '1';
      $book->user_id = auth()->id();
      $book->save();
      
      $notif = new Notification;
      $notif->message = "Anda Mendapat Buku Baru";
      $notif->isClick = '0';
      $notif->isRead = '0';
      $notif->role_id = '3';
      $notif->user_id = auth()->id();
      $notif->book_id = $book->id;
      $notif->save();
      
      // return $notif;
      // var_dump($notif->user_id);
      // exit;
      return redirect(route('books'))->with('status', 'Book was uploaded!');
    }

    public function index()
    {
       if(auth()->user()->isWriter()) {
          $books = Book::where('user_id', auth()->id())->orderBy('id','desc')->get();
       }elseif (auth()->user()->isReviewer()) {
          // $books = Book::where('user_id', auth()->id())->get();
          $books = auth()->user()->books;
       }elseif (auth()->user()->isReviewerIlmiah()) {
          $books = auth()->user()->books;
       }elseif (auth()->user()->isEditor()) {
          $books = Book::all();
       }

      $users = User::allReviewer()->get();
      $ilmiah = User::allReviewerIlmiah()->get();
      return view('books.index', compact(['books','users','ilmiah']));

    }

    public function edit(Book $book)
    {
      return view('books.edit',compact('book'));
    }
    public function nilai(Book $book)
    {
      return view('books.nilai',compact('book'));
    }

    public function setReviewer(Request $request)
    {
      $book = Book::find($request->book);
      $reviewerSyariah = User::find($request->reviewerSyariah);
      $reviewerIlmiah = User::find($request->reviewerIlmiah);
      $book->status=2;
      $book->users()->attach($reviewerSyariah);
      $book->users()->attach($reviewerIlmiah);
      $book->save();

      // $notif = new Notification;
      // $notif->message = "Anda Mendapat Buku Baru Untuk Direview";
      // $notif->isClick = '0';
      // $notif->isRead = '0';
      // $notif->role_id = '4';
      // $notif->user_id = User::find($request->reviewerSyariah);
      // $notif->book_id = $book->id;
      // $notif->save();

      // $notif = new Notification;
      // $notif->message = "Anda Mendapat Buku Baru Untuk Direview";
      // $notif->isClick = '0';
      // $notif->isRead = '0';
      // $notif->role_id = '7';
      // $notif->user_id = User::find($request->reviewerIlmiah);
      // $notif->book_id = $book->id;
      // $notif->save();

      return redirect(route('books'))->with('status', 'Reviewer was set!');
      // return $request->all();
    }

    public function update(Book $book, Request $request)
    {
      $book->title=$request->title;
      if(!auth()->user()->isWriter()) {
        $book->status=$request->status;
      }
      $book->caption=$request->caption;
      $book->save();
      return redirect(route('books'))->with('status', 'Book was updated!');
    }

    public function nilaiIlmiah(Book $book, Request $request)
    {
      $book->title=$request->title;
      $book->caption=$request->caption;
      $book->nilaiIlmiah=$request->nilaiIlmiah;
      $book->save();
      return redirect(route('books'))->with('status', 'Book was updated!');
    }

    public function nilaiSyariah(Book $book, Request $request)
    {
      $book->title=$request->title;
      $book->caption=$request->caption;
      $book->nilaiSyariah=$request->nilaiSyariah;
      $book->save();
      return redirect(route('books'))->with('status', 'Book was reviewed!');
    }
    public function delete(Book $book)
    {
      //return $book;
      $book->delete();
      return redirect(route('books'))->with('status', 'Book was deleted!');
    }
}
