<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title','caption','filename','status','user_id'];

    public function users()
    {
      return $this->belongsToMany(User::class);
    }

    public function notifications()
    {
      return $this->hasMany(Notification::class);
    }

    public function contracts()
    {
      return $this->belongsTo(Contract::class);
    }

    public function sales()
    {
      return $this->belongsToMany(Sale::class);
    }

}
