<?php

namespace App\Notifications;

use App\Chat;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;

class NewChatNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $title;

    protected $body;

    protected $actionName;

    protected $actionUrl;

    public $chat;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Chat  $chat
     * @return void
     */
    public function __construct(Chat $chat)
    {
        $this->chat = $chat;

        $this->title = 'Pesan baru!';
        $this->body = "{$chat->sender->name_limited} mengirimi Anda sebuah pesan.";
        $this->actionName = 'open_url';
        $this->actionUrl = "/contracts?chatId={$chat->sender->id}";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->title,
            'body' => $this->body,
            'action_url' => $this->actionUrl,
            'created' => Carbon::now()->toIso8601String()
        ];
    }

    /**
     * Get the web push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @param  mixed  $notification
     * @return \NotificationChannels\WebPush\WebPushMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title($this->title)
            ->icon('/notif-icon.png')
            ->body($this->body)
            ->action('View Notification', $this->actionName)
            ->data([
                'id' => $notification->id,
                'actionUrl' => $this->actionUrl
            ]);
    }
}
