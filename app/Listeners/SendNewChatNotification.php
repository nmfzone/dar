<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\NewChatNotification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewChatNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->chat->receiver->notify(new NewChatNotification($event->chat));
    }
}
