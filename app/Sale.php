<?php

namespace App;
use App\Book;
use App\Contract;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
  public function books()
  {
    return $this->belongsToMany(Book::class);
  }
}
