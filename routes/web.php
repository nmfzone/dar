<?php


Route::get('/chat', 'ChatController@index');

Route::get('/chat/messages', 'ChatController@getMessages');
Route::get('/sales/upload', 'SaleController@upload')->name('sales.upload');

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard.index');
})->name('dashboard.index');


Route::get('/books/upload', function () {
    return view('books.upload');

})->name('books.upload');

// Route::get('/sales/upload', function () {
//     return view('sales.upload');
//
// })->name('sales.upload');

Route::get('/contracts/upload', 'ContractController@create')->name('contracts.upload');

Route::get('/contracts', function () {
    return view('contracts.index');

})->name('contracts.index');

Route::get('/sales', function () {
    return view('sales.index');

})->name('sales.index');

Route::get('/books/{book}/edit', 'BookController@edit'

)->name('books.edit');

Route::get('/sales/{sale}/edit', 'SaleController@edit'

)->name('sales.edit');

Route::get('/contracts/{book}/edit', 'ContractController@edit'

)->name('contracts.edit');

Route::get('/books/{book}/nilai', 'BookController@nilai'

)->name('books.nilai');

Route::get('/users/{user}/edit', 'UserController@edit'

)->name('users.edit');

Route::put('/books/{book}/edit', 'BookController@update'

)->name('books.update');

Route::put('/contracts/{contract}/edit', 'ContractController@update'

)->name('contracts.update');

Route::put('/books/{book}/nilaiIlmiah', 'BookController@nilaiIlmiah'

)->name('books.nilaiIlmiah');

Route::put('/books/{book}/nilaiSyariah', 'BookController@nilaiSyariah'

)->name('books.nilaiSyariah');

Route::put('/books/{book}/nilai', 'BookController@nilai'

)->name('books.nilai');

Route::post('/books/reviewer', 'BookController@setReviewer'

)->name('reviewer.set');

Route::patch('/users/{user}/edit', 'UserController@update'

)->name('users.update');

Route::delete('/books/{book}/delete','BookController@delete'

)->name('books.delete');

Route::delete('/users/{user}','UserController@delete'

)->name('users.delete');

Route::get('/books', 'BookController@index'
)->name('books');

Route::get('/contracts', 'ContractController@index'
)->name('contracts');

Route::get('/roles','RoleController@index'
)->name('roles');

Route::get('/users','UserController@index'
)->name('users');

Route::get('/users/create','UserController@create'

)->name('users.create');

Route::post('/books', 'BookController@upload')->name('books.store');
Route::post('/sales', 'SaleController@store')->name('sales.store');
Route::post('/contracts', 'ContractController@upload')->name('contracts.store');
Route::post('/users', 'UserController@store')->name('users.store');
// Route::post('/users', 'UserController@store')->name('usersbaru');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Notifications
Route::get('notifications', 'NotificationController@index');
Route::patch('notifications/{id}/read', 'NotificationController@markAsRead');
Route::post('notifications/mark-all-read', 'NotificationController@markAllRead');
Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss');

// Push Subscriptions
Route::post('subscriptions', 'PushSubscriptionController@update');
Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');
