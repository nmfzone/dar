<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        User::create([
            'name' => 'admin',
            'email' => 'admin@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '1',
        ]);
        User::create([
            'name' => 'editor',
            'email' => 'editor@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '3',
        ]);
        User::create([
            'name' => 'reviewer syariah',
            'email' => 'reviewersyariah@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '4',
        ]);
        User::create([
            'name' => 'finance',
            'email' => 'finance@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '5',
        ]);
        User::create([
            'name' => 'marketing',
            'email' => 'marketing@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '6',
        ]);
        User::create([
            'name' => 'sales',
            'email' => 'sales@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '8',
        ]);
        User::create([
            'name' => 'reviewer ilmiah',
            'email' => 'reviewerilmiah@darfiqrin.com',
            'password' => bcrypt('123456'),
            'role_id' => '7',
        ]);
        User::create([
            'name' => 'adi',
            'email' => 'pranata1adi@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => '2',
        ]);

        factory(User::class, 20)->create();
    }
}
