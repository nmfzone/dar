<?php

use Illuminate\Database\Seeder;
use App\Role;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Schema::disableForeignKeyConstraints();
      DB::table('roles')->truncate();
      Schema::enableForeignKeyConstraints();

      $roles = [
          'admin',
          'writer',
          'editor',
          'reviewer',
          'finance',
          'marketing',
          'reviewer ilmiah',
          'sales',
      ];

      foreach ($roles as $key => $value) {
        Role::create(['name' => $value]);
      }
    }
}
