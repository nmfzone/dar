<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("title");
            $table->string("caption");
            $table->string("filename");
            $table->integer('nilaiSyariah')->nullable();
            $table->integer('nilaiIlmiah')->nullable();
            $table->string("status");
            $table->unsignedInteger("user_id")->nullable();
            $table->string('path')->nullable();
            $table->unsignedInteger('role_id')->unsigned()->nullable()->default('2');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
            $table->foreign('role_id')
                  ->references('id')
                  ->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
