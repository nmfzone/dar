<?php

use App\Role;
use Faker\Generator as Faker;


$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('12345678'),
        'remember_token' => str_random(10),
        'role_id' => Role::inRandomOrder()->first()->id,
    ];
});
