<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Chat::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence(20),
        'sender_id' => 2,
        'receiver_id' => User::inRandomOrder()->first()->id,
    ];
});
