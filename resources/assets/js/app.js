
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./url-param');
require('./bootstrap');

window.Vue = require('vue');

import moment from 'moment'
import 'moment/locale/id'
Vue.prototype.moment = moment

Vue.prototype._ = _

import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
  locale: 'id',
  locales: {
    'id': require('date-fns/locale/id')
  }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chatbox', require('./components/chat/Chatbox.vue'));
Vue.component('chat-form', require('./components/chat/ChatForm.vue'));
Vue.component('chat-list', require('./components/chat/ChatList.vue'));
Vue.component('webpush', require('./components/notification/Webpush.vue'));
Vue.component('notification', require('./components/notification/Notification.vue'));
Vue.component('notification-dropdown', require('./components/notification/NotificationDropdown.vue'));
// Vue.component('message', require('./components/chat/Message.vue'));
// Vue.component('user', require('./components/chat/Users.vue'));

const app = new Vue({
    el: '#app'
});

