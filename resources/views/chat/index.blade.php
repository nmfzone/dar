	@if(Auth::user()->isEditor() or Auth::user()->isWriter())
	@extends('layouts.app')

	@section('content')

  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="#">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Chat</li>
      </ol>
    </div><!--/.row-->

    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Chat</h1>
      </div>
    </div><!--/.row-->
    <div class="row" >
			@if(Auth::user()->isWriter())
      <div class="col-md-6" id="app">

        <div class="panel panel-default">
          <div class="panel-heading">
            Kontrak
            <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
          <div class="panel-body">
            <textarea id="summernote" name="editordata"></textarea>
            <div class="input-group">
              <a href="" class="btn btn-primary btn-md" id="btn-todo">Upload</a>
            </div>
          </div>
        </div>
      </div><!--/.col-->
			@endif
      <div class="col-md-6">

        <div class="panel panel-default">
          <div class="panel-heading">
            Daftar User
            <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
          <div class="panel-body">
						<table>
							<tr>
								<th width="200"><em class="percent fa fa-md fa-user color-blue">Penulis</th>
								<th width="120"><em class="percent fa fa-md fa-send color-blue">Aksi</th>
							</tr>
							<tr>
								<td>Ujang</td>
								<td>Chat</td>
							</tr>
						</table>
          </div>
        </div>
      </div><!--/.col-->

      <div class="col-md-6">
        <div class="panel panel-default chat">
          <div class="panel-heading">
            Negosiasi Nilai Kontrak

            <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
          <div class="panel-body" id="app">
            <ul>
              <li class="right clearfix"><span class="chat-img pull-right">
                <img src="http://placehold.it/60/dde0e6/5f6468" alt="User Avatar" class="img-circle" />
                </span>
                <div class="chat-body clearfix">
                  <div class="header"><strong class="pull-left primary-font">Editor</strong> <small class="text-muted">36 mins ago</small></div>
                  <p>@{{message}}</p>
                </div>
              </li>
              <li class="left clearfix"><span class="chat-img pull-left">
                <img src="http://placehold.it/60/30a5ff/fff" alt="User Avatar" class="img-circle" />
                </span>
                <div class="chat-body clearfix">
                  <div class="header"><strong class="primary-font"><user></user></strong> <small class="text-muted">32 mins ago</small></div>
                  <p>Berikut nilai yang saya inginkan.</p>
                </div>
              </li>
              <li class="left clearfix"><span class="chat-img pull-left">
                <img src="http://placehold.it/60/30a5ff/fff" alt="User Avatar" class="img-circle" />
                </span>
                <div class="chat-body clearfix">
                  <div class="header"><strong class="primary-font">John Doe</strong> <small class="text-muted">32 mins ago</small></div>
                  <message></message>
                </div>
              </li>
              <li class="right clearfix"><span class="chat-img pull-right">
                <img src="http://placehold.it/60/dde0e6/5f6468" alt="User Avatar" class="img-circle" />
                </span>
                <div class="chat-body clearfix">
                  <div class="header"><strong class="pull-left primary-font">Editor</strong> <small class="text-muted">36 mins ago</small></div>
                  <p>Silahkan masukkan nilai kontrak yang diharapkan.</p>
                </div>
              </li>
              <li class="left clearfix"><span class="chat-img pull-left">
                <img src="http://placehold.it/60/30a5ff/fff" alt="User Avatar" class="img-circle" />
                </span>
                <div class="chat-body clearfix">
                  <div class="header"><strong class="primary-font">John Doe</strong> <small class="text-muted">32 mins ago</small></div>
                  <p>Berikut nilai yang saya inginkan.</p>
                </div>
              </li>
            </ul>
          </div>
          <div class="panel-footer">
            <div class="input-group">
              <input id="btn-input" type="text" class="form-control input-md" placeholder="Type your message here..." /><span class="input-group-btn">
                <button class="btn btn-primary btn-md" id="btn-chat">Send</button>
            </span></div>
          </div>
        </div>

      </div><!--/.col-->


    </div><!--/.row-->
  </div>	<!--/.main-->

	@endsection
	@endif
