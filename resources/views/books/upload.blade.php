@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dashboard</h1>
		</div>
	</div><!--/.row-->
	<form enctype="multipart/form-data" action="{{route('books.store')}}" method="post">
				@csrf
				{{method_field('post')}}
		<div class="row">
			<div class="col-md-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						Judul*
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<input name="title" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="Judul" required>
						@if ($errors->has('title'))
								<span class="invalid-feedback">
										<strong>{{ $errors->first('title') }}</strong>
								</span>
						@endif
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Keterangan*
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<textarea name="caption" id="summernote" name="editordata" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}"></textarea>
						@if ($errors->has('caption'))
								<span class="invalid-feedback">
										<strong>{{ $errors->first('caption') }}</strong>
								</span>
						@endif
					</div>
				</div>
			</div><!--/.col-->
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Upload Dokumen*
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<input name="filename" type="file" >
					</div>
				</div>
		</div><!--/.row-->
		<div class="col-md-12">
			<div class="input-group">
				<button type="submit" class="btn btn-primary btn-md" id="btn-todo">Upload</button>
			</div>
		</div>

		</div>	<!--/.main-->
	</form>
@endsection
