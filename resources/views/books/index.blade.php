@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dashboard</h1>
		</div>
	</div><!--/.row-->
	<div class="row">

				<div class="col-md-6">
					@if (session('status'))
							<div class="alert alert-info"><a  href="#" data-dismiss="alert"class="pull-right"><em class="fa fa-lg fa-close"></em></a>
									{{ session('status') }}
							</div>
					@endif
					<div class="panel panel-default ">
						<div class="panel-heading">
							Daftar Naskah
							<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
						<div class="panel-body timeline-container">
							<table class="table" width="100%">
								<thead>
									<tr>
										<th>Nama Buku</th>
										@if(Auth::user()->isReviewer() or Auth::user()->isReviewerIlmiah() or Auth::user()->isEditor() or Auth::user()->isWriter())
											<th width="300">Status</th>
										@endif
										<th >&nbsp;</th>
									</tr>
									<tbody>
										@foreach($books as $book)
										<tr>
											<td width="700">{{$book->title}}</td>
											@if(Auth::user()->isReviewer() or Auth::user()->isReviewerIlmiah() or Auth::user()->isEditor() )
											<td>
												@if($book->status==1)
													{{"Belum Dibaca"}}
												@elseif($book->status==2)
													{{"Naskah Direview"}}
												@elseif($book->status==3)
													{{"Naskah Ditolak"}}
												@elseif($book->status==4)
													{{"Revisi Naskah"}}
												@else($book->status==5)
													{{"Naskah Selesai"}}
												@endif
											</td>
											@endif

											@if(Auth::user()->isWriter())
											<td width="100">
												@if($book->status==0)
													{{"Batal Kontrak"}}
												@elseif($book->status==1)
													{{"Sedang Diedit"}}
												@elseif($book->status==2)
													{{"Sedang Direview"}}
												@elseif($book->status==3)
													{{"Naskah Ditolak"}}
												@elseif($book->status==4)
													{{"Silahkan Revisi"}}
												@elseif($book->status==5)
													{{"Naskah Diterima Silahkan Mengakses Menu Kontrak"}}
												@elseif($book->status==6)
													{{"Proses Penjualan"}}
												@elseif($book->status==7)
													{{"Penerbitan Selesai"}}
												@endif
											</td>
											@endif

											@if(!Auth::user()->isReviewer() and !Auth::user()->isReviewerIlmiah())
											<td width="100" align="right"><a class="btn btn-warning" href="{{route ('books.edit', $book)}}">Edit</a></td>
											@endif
											@if(Auth::user()->isReviewer() or Auth::user()->isReviewerIlmiah())
											<td width="200"><a class="btn btn-primary" href="{{route ('books.nilai', $book)}}">Nilai</a></td>
											@endif
											<td width="100">
												@if(Auth::user()->isWriter() && $book->status<5 )
												<a class="btn btn-danger" href="{{route('books.delete', $book)}}"
														onclick="event.preventDefault();
																	document.getElementById('books-delete').submit();"
														>Hapus
												</a>
												@endif
											</td>

										</tr>

										<form id="books-delete" action="{{ route('books.delete', $book) }}" method="POST" style="display: none;">
											@method('DELETE')
											@csrf
			              </form>
										@endforeach
									</tbody>
								</thead>
							</table>

						</div>
					</div>
				</div><!--/.col-->
				<div class="col-md-6">

					@if(Auth::user()->isEditor())
					<div class="panel panel-default ">
						<div class="panel-heading">
							Distribusi Naskah
							<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
							<div class="panel-body timeline-container">
								<form class="" action="{{route('reviewer.set')}}" method="post">
									@method('POST')
					         @csrf
									<ul class="timeline">
										<li>
											<div class="timeline-badge"><em class="glyphicon glyphicon-book"></em></div>
											<div class="timeline-panel">
												<div class="timeline-heading">
													<h4 class="timeline-title">Buku</h4>
												</div>
												<div class="timeline-body">
			                    <div class="panel-body">
			                      <select class="form-control" name="book">															
															<option selected="selected">Silahkan Pilih</option>
															@foreach($books as $book)
			                        <option value="{{$book->id}}">{{$book->title}}</option>
															@endforeach
			                      </select>
			                    </div>
												</div>
											</div>
										</li>
										<li>
											<div class="timeline-badge"><em class="glyphicon glyphicon-user"></em></div>
											<div class="timeline-panel">
												<div class="timeline-heading">
													<h4 class="timeline-title">Reviewer Syariah</h4>
												</div>
												<div class="timeline-body">
			                    <div class="panel-body">
			                      <select class="form-control" name="reviewerSyariah">
															<option selected="selected">Silahkan Pilih</option>
															@foreach($users as $user)
															<option value="{{$user->id}}">{{$user->name}}</option>
															@endforeach
			                      </select>
			                    </div>
			                  </div>
											</div>
										</li>
										<li>
											<div class="timeline-badge"><em class="glyphicon glyphicon-user"></em></div>
											<div class="timeline-panel">
												<div class="timeline-heading">
													<h4 class="timeline-title">Reviewer Ilmiah</h4>
												</div>
												<div class="timeline-body">
			                    <div class="panel-body">
			                      <select class="form-control" name="reviewerIlmiah">
															<option selected="selected">Silahkan Pilih</option>
															@foreach($ilmiah as $user)
															<option value="{{$user->id}}">{{$user->name}}</option>
															@endforeach
			                      </select>
			                    </div>
			                  </div>
											</div>
										</li>
									</ul>
									<div class="input-group">
			      				<button class="btn btn-primary btn-md" type="submit" >Bagikan</button>
			      			</div>
								</form>
							</div>
					</div>
					@endif
				</div><!--/.col-->
				</div><!--/.col-->
			</div><!--/.row-->

@endsection
