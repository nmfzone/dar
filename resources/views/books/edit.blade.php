@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dashboard</h1>
		</div>
	</div><!--/.row-->
	<form action="{{route('books.update', $book)}}" method="post">
       @method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Judul
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						@if(!Auth::user()->isReviewer() and !Auth::user()->isReviewerIlmiah())
						<input name="title" class="form-control" placeholder="Judul" value="{{$book->title}}">
						@endif
						@if(Auth::user()->isReviewer() or Auth::user()->isReviewerIlmiah())
							<input name="title" class="form-control" placeholder="Judul" value="{{$book->title}}" disabled>
						@endif
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						@if(Auth::user()->isReviewer() or Auth::user()->isReviewerIlmiah())
						Catatan
						@endif
						@if(!Auth::user()->isReviewer() and !Auth::user()->isReviewerIlmiah())
						Keterangan
						@endif
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<textarea name="caption" id="summernote" name="editordata" >{{$book->caption}}</textarea>
					</div>
				</div>
			</div><!--/.col-->
			@if(Auth::user()->isEditor() and Auth::user()->isWriter())
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Upload Dokumen
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<input name="filename" type="file" name="" value="">
					</div>
				</div>
		</div><!--/.row-->
		@endif
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Download Dokumen
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<a href="/storage/{{$book->path}}">Download</a>
					</div>
				</div>
		</div><!--/.row-->
		@if(Auth::user()->isEditor())
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Penilaian Editor
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<select class="form-control" name="status">
							<option value="">--Silahkan Pilih--</option>
							<option value="5">Diterima</option>
							<option value="4">Diterima Dengan Catatan</option>
							<option value="3">Ditolak</option>
						</select>
					</div>
				</div>
		</div><!--/.row-->
		@endif
		@if(Auth::user()->isEditor())
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Penilaian Reviewer
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<table>
							<tr>
								<th width="200">Reviewer</th>
								<th width="200">Penilaian</th>
							</tr>
							<tr>
								<td width="200">Reviewer Syariah</td>
								<td width="200">
										@if($book->nilaiSyariah==1)
											{{"Diterima"}}
										@elseif($book->nilaiSyariah==2)
											{{"Diterima Dengan Catatan"}}
										@else($book->nilaiSyariah==3)
											{{"Ditolak"}}
										@endif
								</td>
							</tr>
							<tr>
								<td width="200">Reviewer Ilmiah</td>
								<td width="200">
									@if($book->nilaiIlmiah==1)
										{{"Diterima"}}
									@elseif($book->nilaiIlmiah==2)
										{{"Diterima Dengan Catatan"}}
									@else($book->nilaiIlmiah==3)
										{{"Ditolak"}}
									@endif
								</td>
							</tr>

						</table>
					</div>
				</div>
		</div><!--/.row-->
		@endif
		<div class="col-md-12">
			<div class="input-group">
				<button type="submit" class="btn btn-primary btn-md" id="btn-todo">Edit</button>
			</div>
		</div>


	</div>	<!--/.main-->
	</form>
@endsection
