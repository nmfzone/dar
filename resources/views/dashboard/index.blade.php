	@extends('layouts.app')
	@section('content')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Dashboard</h1>
				@if (session('status'))
				    <div class="alert alert-info"><a  href="#" data-dismiss="alert"class="pull-right"><em class="fa fa-lg fa-close"></em></a>
				        {{ session('status') }}
				    </div>
				@endif
			</div>
		</div><!--/.row-->

		@if(!Auth::user()->isAdmin())
		<div class="row">
			@if(!Auth::user()->isReviewer() and !Auth::user()->isSales() and !Auth::user()->isReviewerIlmiah() and  !Auth::user()->isEditor() and !Auth::user()->isMarketing() and !Auth::user()->isFinance())
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Upload Naskah</h4>
						<div class="easypiechart" class="large"><em class="percent fa fa-xl fa-upload color-blue"><br><a href="{{ route('books.upload') }}" class="color-blue">Upload</a></em></div>
					</div>
				</div>
			</div>
			@endif
			@if(!Auth::user()->isSales())
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Lihat Naskah</h4>
						<div class="easypiechart"><em class="percent fa fa-xl fa-clipboard color-teal"><br><a href="{{ route('books') }}" class="color-teal">Lihat</a></em></div>
					</div>
				</div>
			</div>
			@endif
			@if(!Auth::user()->isReviewer() and !Auth::user()->isReviewerIlmiah() and  !Auth::user()->isEditor())
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Penjualan</h4>
						<div class="easypiechart"><em class="percent fa fa-xl fa-bar-chart color-orange"><br><a href="{{ route('sales.index')}}" class="color-orange">Penjualan</a></em></div>
					</div>
				</div>
			</div>
			@endif
			@if(!Auth::user()->isSales())
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Buku</h4>
						<div class="easypiechart"><em class="percent fa fa-xl fa-book color-blue"><br><a href="" class="color-blue">Buku</a></em></div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		@endif
@endif
@if(Auth::user()->isWriter())
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default ">
					<div class="panel-heading">
						Timeline
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body timeline-container">
						<ul class="timeline">
							<li>
								<div class="timeline-badge"><em class="glyphicon glyphicon-pushpin"></em></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Upload Naskah</h4>
									</div>
									<div class="timeline-body">
										<p>Naskah di upload</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge primary"><em class="glyphicon glyphicon-link"></em></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Proses Pemeriksaan Naskah</h4>
									</div>
									<div class="timeline-body">
										<p>Naskah sedang diperiksa oleh editor dan di review oleh reviewer.</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge"><em class="glyphicon glyphicon-camera"></em></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Revisi</h4>
									</div>
									<div class="timeline-body">
										<p>Pengguna melakukan revisi.</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge"><em class="glyphicon glyphicon-paperclip"></em></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Negosiasi Kontrak</h4>
									</div>
									<div class="timeline-body">
										<p>Kedua belah pihak melakukan negosiasi kontrak.</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge"><em class="glyphicon glyphicon-paperclip"></em></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Tanda Tangan Kontrak</h4>
									</div>
									<div class="timeline-body">
										<p>Kedua belah pihak setuju dengan kontrak yang sudah disediakan.</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div><!--/.col-->
	@endif
	@if(!Auth::user()->isSales())
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Calendar
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<div id="calendar"></div>
					</div>
				</div>
			</div>
			@endif
			</div><!--/.col-->
		</div><!--/.row-->
	</div>	<!--/.main-->

	@endsection
