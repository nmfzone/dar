@if(Auth::user()->isEditor() or Auth::user()->isWriter())
@extends('layouts.app')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#">
        <em class="fa fa-home"></em>
      </a></li>
      <li class="active">Kontrak</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Kontrak</h1>
    </div>
  </div><!--/.row-->
  <div class="row">
    <div class="col-md-6">
      <form class="" action="{{route('contracts.update',['contract'=> $contract->id])}}" method="post">
        @csrf
        @method('PUT')
      <div class="panel panel-default">
        <div class="panel-heading">
          <?php $book = $contract->book ?>
          {{ $book->title }}
          <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
        <div class="panel-body">
          <textarea id="summernote" name="kontrak">{{$contract->kontrak}}</textarea>
            <br>
            Nilai Royalti
            <br>
            <input class="form-control" placeholder="%" value="{{$contract->royalti}}" name="royalti" required></input>
            <br>
            Hasil
            <br>
            <select class="form-control" name="status">
              <option value="7">Setuju</option>
              <option value="8">Tidak Setuju</option>
            </select>            <br>
            @if(Auth::user()->isEditor())
            <div class="input-group">
              <button type="submit" class="btn btn-primary btn-md" id="btn-todo">Upload</button>
            </div>
            @endif
					</div>
        </div>
      </div>
    </div><!--/.col-->
  </div>
  </form>

</div>	<!--/.main-->

@endsection
@endif
