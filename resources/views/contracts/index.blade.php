@if(Auth::user()->isEditor() or Auth::user()->isWriter())
@extends('layouts.app')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#">
        <em class="fa fa-home"></em>
      </a></li>
      <li class="active">Kontrak</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Kontrak</h1>
    </div>
  </div><!--/.row-->
  <div class="row">
    <div class="col-md-6">
      <form class="" action="{{route('contracts.store')}}" method="post">
        @csrf
        {{ method_field('post') }}
        <div class="panel panel-default">
          <div class="panel-heading">
            Kontrak
            <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
          </div>
          <div class="panel-body">
            <table width="100%" class="table">
              <thead>
                <tr>
                  <th width="">Buku</th>
                  <th width="">Kontrak</th>
                  <th width="100"></th>
                </tr>
              </thead>
              <tbody>
                @foreach($books as $book)
                <tr>
                <td>{{$book->title}}</td>
                <td>{{$book->title}}</td>
                <td>
                  <a class="btn btn-primary" href="{{route('contracts.edit', ['book'=>$book->id])}}">Lihat</a>
                </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </form>
    </div><!--/.col-->
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Chats
          <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
        </div>
        <div class="panel-body">
          <chat-list></chat-list>
        </div>
      </div>
    </div>
  </div>
</div>	<!--/.main-->

@endsection
@endif
