@if(Auth::user()->isEditor() or Auth::user()->isWriter())
@extends('layouts.app')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#">
        <em class="fa fa-home"></em>
      </a></li>
      <li class="active">Kontrak</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Kontrak</h1>
    </div>
  </div><!--/.row-->
  <div class="row">
    <div class="col-md-6">
      <form class="" action="{{route('contracts.store')}}" method="post">
        @csrf
				{{method_field('post')}}
      <div class="panel panel-default">
        <div class="panel-heading">
          <?php $book = session('book') ?>
          {{ $book->title }}
          <input type="hidden" name="book_id" value="{{$book->id}}">
          <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
        <div class="panel-body">
          <textarea id="summernote" name="kontrak"></textarea>
          @if(Auth::user()->isEditor())
            <br>
            Nilai Royalti
            <br>
            <input class="form-control" placeholder="%" name="royalti" required></input>
					</div>

          <div class="input-group">
            <button type="submit" class="btn btn-primary btn-md" id="btn-todo">Upload</button>
          </div>
          @endif
        </div>
      </div>
    </div><!--/.col-->
  </div>
  </form>

</div>	<!--/.main-->

@endsection
@endif
