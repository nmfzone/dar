<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Darfiqrin') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
  	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-notifications.css') }}" rel="stylesheet">
  	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
  	<link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
  	<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
  	<link href="{{ asset('summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <webpush></webpush>
      	@include('layouts.navbar')

        @if (auth()->check() && !isset($showSidebar))
      	    @include('layouts.sidebar')
        @endif

        <!-- <main class="py-4"> -->
            @yield('content')
        <!-- </main> -->
    </div>
    <!-- Scripts -->
    <script>
      window.App = {!! json_encode([
        'csrfToken' => csrf_token(),
        'user' => auth()->user(),
        'vapidPublicKey' => config('webpush.vapid.public_key'),
      ]) !!};
    </script>
    <script src="{{ mix('js/app.js') }}" defer></script>

    	<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
    	<script src="{{ asset('js/chart.min.js') }}"></script>
    	<script src="{{ asset('js/chart-data.js') }}"></script>
    	<script src="{{ asset('js/easypiechart.js') }}"></script>
    	<script src="{{ asset('js/easypiechart-data.js') }}"></script>
    	<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    	<script src="{{ asset('js/custom.js') }}"></script>
    	<script src="{{ asset('summernote/summernote.js') }}"></script>
    	<script>
    		window.onload = function () {
    	var chart1 = document.getElementById("line-chart").getContext("2d");
    	window.myLine = new Chart(chart1).Line(lineChartData, {
    	responsive: true,
    	scaleLineColor: "rgba(0,0,0,.2)",
    	scaleGridLineColor: "rgba(0,0,0,.05)",
    	scaleFontColor: "#c5c7cc"
    	});
    };
    	</script>
      <script>
          $(document).ready(function() {
              $('#summernote').summernote({
                height: 200,
              });
              showHideRemoveButton();

              $('.btn-add-book').click(function() {
                var template = $('.book-wrap:first').clone();
                $('.book-wrap:last').after(template);
                showHideRemoveButton();
              });

              $('.book-container').on('click', '.btn-remove-book', function() {
                $(this).parent().parent().remove();
                showHideRemoveButton();
              });

              function showHideRemoveButton() {
                var count = $('.book-wrap').length;
                if (count > 1) {
                  $('.btn-remove-book').show();
                } else {
                  $('.btn-remove-book').hide();
                }
              }
          });
        </script>
        <footer>
          <div class="col-sm-12">
            <p class="back-link">Create by <a href="https://www.linkedin.com/in/mohamad-waskita-adi-pranata-905437105/">Adi</a></p>
          </div>
        </footer>
</body>
</html>
