
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <div class="profile-sidebar">
    <div class="profile-userpic">
      <img src="ada.jpg" class="img-responsive" alt="">
    </div>
    <div class="profile-usertitle">
      <div class="profile-usertitle-name">@auth{{Auth::user()->name }}@endauth</div>
      <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="divider"></div>
  <form role="search">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Search">
    </div>
  </form>
  <ul class="nav menu">
    <li class=""><a href="{{route('dashboard.index')}}"><em class="fa fa-dashboard">&nbsp;</em>Dashboard</a></li>
    @if(Auth::user()->isAdmin())
    <li><a href="{{route('users')}}"><em class="fa fa-user">&nbsp;</em> User</a></li>
    @endif
    @if(!Auth::user()->isAdmin() and !Auth::user()->isEditor() and !Auth::user()->isReviewerIlmiah() and  !Auth::user()->isReviewer() )
    <li><a href="#"><em class="fa fa-bar-chart">&nbsp;</em>Penjualan</a></li>
    @endif
    @if(!Auth::user()->isAdmin() and !Auth::user()->isSales())
    <li><a href="#"><em class="fa fa-book">&nbsp;</em>Buku</a></li>
    @endif
    @if(Auth::user()->isEditor() or Auth::user()->isWriter())
    <li><a href="{{route('contracts')}}"><em class="fa fa-file">&nbsp;</em>Kontrak</a></li>
    @endif
    @if(Auth::user()->isEditor())
    <li><a href="#"><em class="fa fa-list">&nbsp;</em>Naskah</a></li>
    @endif
    <li>
      <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <em class="fa fa-power-off">&nbsp;</em>Logout
      </a>
      </li>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
  </ul>
</div><!--/.sidebar-->
