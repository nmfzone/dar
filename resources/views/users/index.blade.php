@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#">
        <em class="fa fa-home"></em>
      </a></li>
      <li class="active">Users</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Users</h1>
    </div>
  </div>

  <div class="col-md-12">
    <div class="panel panel-default ">
      @if (session('status'))
          <div class="alert alert-info"><a  href="#" data-dismiss="alert"class="pull-right"><em class="fa fa-lg fa-close"></em></a>
              {{ session('status') }}
          </div>
      @endif
      <div class="panel-heading">
        Daftar Staff
        <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
        <a href="{{route('users.create')}}" class="btn btn-success pull-right">Tambah Staff</a>
      </div>
      <div class="panel-body timeline-container">
        <table>
          <thead>
            <tr>
              <th width="200">Nama</th>
              <th width="200">Email</th>
              <th width="100">Jabatan</th>
              <th width="200">&nbsp;</th>
            </tr>
            <tbody>
              @foreach($users as $user)
              <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role->name}}</td>
                <td>
                  <a class="btn btn-warning" href="{{route('users.edit',$user)}}" name="button">Edit</a>
                  <form action="{{route('users.delete', $user) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger" type="submit">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
