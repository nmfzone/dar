@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#">
        <em class="fa fa-home"></em>
      </a></li>
      <li class="active">Users/create</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Tambah Users</h1>
    </div>
  </div>


  <form action="{{route('users.store')}}" method="POST">
				@csrf
		<div class="row">
			<div class="col-md-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						Tambah Staff
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
            Nama <br>
						<input name="name" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="Nama" required></input><br>
            Email<br>
						<input name="email" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="email" required></input><br>
            Password<br>
						<input name="password" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="password" type="password" required></input><br>
            Konfirmasi Password<br>
						<input name="password_confirmation" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="password" type="password"required></input><br>
            Role<br>
						<select name="role_id" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="role" required>
                <option value="">-- Pilih Role --</option>
                @foreach ($roles as $role)
                <option value="{{$role->id}}"> {{$role->name}} </option>
              @endforeach
            </select><br>
            <button class="btn btn-primary" type="submit" name="button">Simpan</button>
					</div>
				</div>

			</div><!--/.col-->

	</div>	<!--/.main-->
	</form>
</div>
@endsection
