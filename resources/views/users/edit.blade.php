@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#">
        <em class="fa fa-home"></em>
      </a></li>
      <li class="active">Users/edit</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Edit Users</h1>
    </div>
  </div>

  <form action="{{route('users.update', $user)}}" method="POST">
        @method('PATCH')
				@csrf
		<div class="row">
			<div class="col-md-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						Edit Staff
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
            Nama <br>

						<input name="name" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="Nama" required value="{{$user->name}}"></input><br>
            Email<br>
            @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif

						<input name="email" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="email" required value="{{$user->email}}"disabled></input><br>
            Password<br>
            @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

						<input name="password" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="password" type="password" required></input><br>
            Konfirmasi Password<br>
            @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

						<input name="password_confirmation" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="password" type="password"required></input><br>
            Role<br>
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif

						<select name="role_id" class="form-control{{ $errors->has('caption') ? ' is-invalid' : '' }}" placeholder="role" required>
                <option value="">-- Pilih Role --</option>
                @foreach ($roles as $role)
                <option value="{{$role->id}}"> {{$role->name}} </option>
              @endforeach
            </select><br>
            @if ($errors->has('role_id'))
                <span class="help-block">
                  <strong>{{ $errors->first('role_id') }}</strong>
                </span>
            @endif
            <button class="btn btn-primary" type="submit">Simpan</button>
					</div>
				</div>

			</div><!--/.col-->

	</div>	<!--/.main-->
	</form>


</div>
@endsection
