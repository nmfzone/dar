  @extends('layouts.app')
  @section('content')
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="#">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Penjualan</li>
      </ol>
    </div><!--/.row-->

    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Penjualan</h1>
        @if (session('status'))
            <div class="alert alert-info"><a  href="#" data-dismiss="alert"class="pull-right"><em class="fa fa-lg fa-close"></em></a>
                {{ session('status') }}
            </div>
        @endif
      </div>
    </div><!--/.row-->
  @if(Auth::user()->isSales())
  <div class="col-xs-6 col-md-3">
    <div class="panel panel-default">
      <div class="panel-body easypiechart-panel">
        <h4>Upload Penjualan</h4>
        <div class="easypiechart" class="large"><em class="percent fa fa-xl fa-upload color-blue"><br><a href="{{ route('sales.upload') }}" class="color-blue">Upload</a></em></div>
      </div>
    </div>
  </div>
  </div>
  @endif
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Penjualan
          <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
        <div class="panel-body">
          <table width="100%" class="table">
            <thead>
              <tr>
                <th width="">Buku</th>
                <th width="">Terjual</th>
                <th width="">Royalti</th>
                <th width="">Royalti Terbayar</th>
              </tr>
            </thead>
            <tbody>
              <tr>
              <td>haha</td>
              <td>200</td>
              <td>Rp.200.000</td>
              <td>Rp.200.000</td>
              </tr>
            </tbody>
          </table>
					</div>
        </div>
      </div>
    </div><!--/.col-->
  @endsection
