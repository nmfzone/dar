@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dashboard</h1>
		</div>
	</div><!--/.row-->

	<form action="{{route('sales.store')}}" method="post">
		@csrf
		{{method_field('post')}}
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Buku*
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body book-container">
            <div class="row book-wrap">
              <div class="col-md-4 form-group" >
                <select class="form-control" name="judul[]">
                  @foreach($books as $book)
                  <option value="{{$book->id}}">{{$book->title}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3 form-group" >
                <input class="form-control" type="number" name="jumlah[]" value="" min="1" placeholder="jumlah">
              </div>
              <div class="col-md-3 form-group" >
                <input class="form-control" type="number" name="harga[]" value="" min="0" placeholder="harga">
              </div>
              <div class="col-md-2">
                <button type="button" class="btn btn-danger btn-remove-book"><i class="fa fa-trash"></i></button>
              </div>
            </div>
            <a class="btn btn-primary btn-md btn-add-book">Tambah</a>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Tanggal*
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body form-group">
						<div class="row">
							<div class="col-md-6">
								<input class="form-control"type="date" name="tanggal" value="">
							</div>
						</div>
					</div>
				</div>
			</div><!--/.col-->
		<div class="col-md-12">
			<div class="input-group">
				<button type="submit" class="btn btn-primary btn-md" id="btn-todo">Upload</button>
			</div>
		</div>
		</div>	<!--/.main-->
	</form>
@endsection
