@extends('layouts.app')

@section('content')

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Roles</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Roles</h1>
      </div>
    </div>

    <div class="col-md-6">
      <div class="panel panel-default ">
        <div class="panel-heading">
          Daftar Role
          <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
        <div class="panel-body timeline-container">
          <table>
            <thead>
              <tr>
                <th width="100">Role</th>
                <th width="200">&nbsp;</th>
              </tr>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td><button class="btn btn-warning" type="button" name="button">Edit</button>
                  <button class="btn btn-danger" type="button" name="button">Hapus</button></td>
                </tr>
              </tbody>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
